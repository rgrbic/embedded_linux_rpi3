# embedded_linux_rpi3

Raspberry Pi 3 embedded linux. Made with Buildroot: uboot, kernel, rootfs (busybox)<br/>

Buildroot: 2018.11. <br/>
U-boot: 2018.07. <br/>
Kernel: 4.14.y (https://github.com/raspberrypi/linux/commit/6d27aa156c26977dfd079a7107e31670127d17d3)<br/>